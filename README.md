SISTEMA DE GESTIÓN DE NÓMINA 2020
=================================

ARQUITECTURA
-------------
- Modelo, Vista, Controlador

HERRAMIENTAS
-------------
- Lenguaje de programación PHP 7.3
- Base de datos MySql/MariaDB 8.0

PASOS PARA IMPLEMENTAR EL PROYECTO:
-----------------------------------
- La implementación del proyecto se lo realiza con un contenedor Docker Compose con los siguientes pasos:
- Instalamos Docker: apt install docker.io
- Instalamos Docker Compose: apt install docker-compose
- En la carpeta que se desee descargar el proyecto escribimos: git clone https://gitlab.com/gustavoguaigua/nomina_gus.git
- Ingresamos a la carpeta descargada: cd nomina_gus
- ejecutamos el docker compose: docker-compose up
- Esperamos un minuto que se creen las imágenes y se despliegue el contenedor
- Creamos la base de datos con el script: sql/Crud-Mysql-php.sql
- user="root"; password=".sweetpwd."; puerto=8082
- Abrimos un browser de internet y escribimos: http://192.168.100.250:8010/index.php

- gga - 2020