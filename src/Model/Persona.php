<?php 
error_reporting(E_ALL ^ E_NOTICE);
/**
* 
*/
class Persona
{
	private $id;
	private $nombres;
	private $apellidos;
	private $estado;

	
	function __construct($id, $nombres,$apellidos, $estado)
	{
		$this->setId($id);
		$this->setNombres($nombres);
		$this->setApellidos($apellidos);
		$this->setEstado($estado);		
	}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNombres(){
		return $this->nombres;
	}

	public function setNombres($nombres){
		$this->nombres = $nombres;
	}

	public function getApellidos(){
		return $this->apellidos;
	}

	public function setApellidos($apellidos){
		$this->apellidos = $apellidos;
	}

	public function getEstado(){

		return $this->estado;
	}

	public function setEstado($estado){
		
		if (strcmp($estado, 'on')==0) {
			$this->estado=1;
		} elseif(strcmp($estado, '1')==0) {
			$this->estado='checked';
		}elseif (strcmp($estado, '0')==0) {
			$this->estado='off';
		}else {
			$this->estado='0';
		}

	}

	public static function save($persona){
		$db=Db::getConnect();
		//var_dump($persona);
		//die();
		

		$insert=$db->prepare('INSERT INTO persona VALUES (NULL, :nombres,:apellidos,:estado)');
		$insert->bindValue('nombres',$persona->getNombres());
		$insert->bindValue('apellidos',$persona->getApellidos());
		$insert->bindValue('estado',$persona->getEstado());
		$insert->execute();
	}

	public static function all(){
		$db=Db::getConnect();
		$listaPersonas=[];

		$select=$db->query('SELECT * FROM persona order by id');

		foreach($select->fetchAll() as $persona){
			$listaPersonas[]=new Persona($persona['id'],$persona['nombres'],$persona['apellidos'],$persona['estado']);
		}
		return $listaPersonas;
	}

	public static function searchById($id){
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM persona WHERE id=:id');
		$select->bindValue('id',$id);
		$select->execute();

		$personaDb=$select->fetch();


		$persona = new Persona ($personaDb['id'],$personaDb['nombres'], $personaDb['apellidos'], $personaDb['estado']);
		//var_dump($persona);
		//die();
		return $persona;

	}

	public static function update($persona){
		$db=Db::getConnect();
		$update=$db->prepare('UPDATE persona SET nombres=:nombres, apellidos=:apellidos, estado=:estado WHERE id=:id');
		$update->bindValue('nombres', $persona->getNombres());
		$update->bindValue('apellidos',$persona->getApellidos());
		$update->bindValue('estado',$persona->getEstado());
		$update->bindValue('id',$persona->getId());
		$update->execute();
	}

	public static function delete($id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE  FROM persona WHERE id=:id');
		$delete->bindValue('id',$id);
		$delete->execute();		
	}
}

?>