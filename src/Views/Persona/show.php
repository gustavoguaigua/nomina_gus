
<div class="container">
	<h2>Lista Personal en Nómina</h2>
	<form class="form-inline" action="?controller=Persona&action=search" method="post">
		<div class="form-group row">
			<div class="col-xs-4">
				<input class="form-control" id="id" name="id" type="text" placeholder="Busqueda por ID">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary" ><span class="glyphicon glyphicon-search"> </span> Buscar</button>
			</div>
		</div>
	</form>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Estado</th>
					<th>Accion</th>
				</tr>
				<tbody>
					<?php foreach ($listaPersonas as $persona) {?>

					
					<tr>
						<td> <a href="?controller=Persona&&action=updateshow&&idPersona=<?php  echo $persona->getId()?>"> <?php echo $persona->getId(); ?></a> </td>
						<td><?php echo $persona->getNombres(); ?></td>
						<td><?php echo $persona->getApellidos(); ?></td>
						<td><?php if ( $persona->getEstado()=='checked'):?>
							Activo
						<?php  else:?>
							Inactivo
						<?php endif; ?></td>
						<td><a href="?controller=Persona&&action=delete&&id=<?php echo $persona->getId() ?>">Eliminar</a> </td>
					</tr>
					<?php } ?>
				</tbody>

			</thead>
		</table>

	</div>	

</div>