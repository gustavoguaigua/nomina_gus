<?php 
/**
* 
*/
class PersonaController
{
	
	function __construct()
	{
		
	}

	function index(){
		require_once('Views/Persona/bienvenido.php');
	}

	function register(){
		require_once('Views/Persona/register.php');
	}

	function save(){
		if (!isset($_POST['estado'])) {
			$estado="of";
		}else{
			$estado="on";
		}
		$persona= new Persona(null, $_POST['nombres'],$_POST['apellidos'],$estado);

		Persona::save($persona);
		$this->show();
	}

	function show(){
		$listaPersonas=Persona::all();

		require_once('Views/Persona/show.php');
	}

	function updateshow(){
		$id=$_GET['idPersona'];
		$persona=Persona::searchById($id);
		require_once('Views/Persona/updateshow.php');
	}

	function update(){
		$persona = new Persona($_POST['id'],$_POST['nombres'],$_POST['apellidos'],$_POST['estado']);
		Persona::update($persona);
		$this->show();
	}
	
	function delete(){
		$id=$_GET['id'];
		Persona::delete($id);
		$this->show();
	}

	function search(){
		if (!empty($_POST['id'])) {
			$id=$_POST['id'];
			$persona=Persona::searchById($id);
			$listaPersonas[]=$persona;
			//var_dump($id);
			//die();
			require_once('Views/Persona/show.php');
		} else {
			$listaPersonas=Persona::all();

			require_once('Views/Persona/show.php');
		}
		
		
	}

	function error(){
		require_once('Views/Persona/error.php');
	}

}

?>